package com.godeltech.springreactiver2dbc.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
@JsonDeserialize(builder = TodoDto.TodoDtoBuilder.class)
public class TodoDto {

    Integer id;

    String text;

    @JsonPOJOBuilder(withPrefix = "")
    public static class TodoDtoBuilder {
    }
}
