package com.godeltech.springreactiver2dbc.entity;

import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;

@Value
@Builder(toBuilder = true)
public class Todo {

    @Id
    Integer id;

    String text;
}
