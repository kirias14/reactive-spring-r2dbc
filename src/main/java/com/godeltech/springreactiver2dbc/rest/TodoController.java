package com.godeltech.springreactiver2dbc.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.godeltech.springreactiver2dbc.dto.TodoDto;
import com.godeltech.springreactiver2dbc.service.TodoService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class TodoController {

    private final TodoService todoService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = TodoDto.class))) })
    @Operation(description = "Get todos")
    @GetMapping(value = "/todo", produces = APPLICATION_JSON_VALUE)
    public Flux<TodoDto> getTodos() {
        return todoService.getTodos();
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = TodoDto.class))) })
    @Operation(description = "Get todo")
    @GetMapping(value = "/todo/{id}", produces = APPLICATION_JSON_VALUE)
    public Mono<TodoDto> getTodo(@PathVariable Integer id) {
        return todoService.getTodo(id);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = TodoDto.class))) })
    @Operation(description = "Update todo")
    @PutMapping(value = "/todo/{id}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public Mono<TodoDto> updateTodo(@PathVariable Integer id, @RequestBody TodoDto item) {
        return todoService.update(id, item);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created", content = @Content(schema = @Schema(implementation = TodoDto.class))) })
    @Operation(description = "Create todo")
    @PostMapping(value = "/todo", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public Mono<TodoDto> createTodo(@RequestBody TodoDto item) {
        return todoService.create(item);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK") })
    @Operation(description = "Remove todo")
    @DeleteMapping(value = "/todo/{id}")
    public Mono<ResponseEntity<Void>> remove(@PathVariable Integer id) {
        return todoService.remove(id)
                .thenReturn(ResponseEntity.ok().build());
    }
}
