package com.godeltech.springreactiver2dbc.repository;

import com.godeltech.springreactiver2dbc.entity.Todo;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TodoRepository {
    Mono<Todo> get(Integer id);

    Flux<Todo> getAll();

    Mono<Todo> create(Todo item);

    Mono<Void> delete(Integer id);

    Mono<Todo> update(Integer id, Todo transform);
}
