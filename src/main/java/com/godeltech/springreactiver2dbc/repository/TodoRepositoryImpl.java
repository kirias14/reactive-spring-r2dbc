package com.godeltech.springreactiver2dbc.repository;

import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.data.r2dbc.query.Criteria;
import org.springframework.stereotype.Repository;

import com.godeltech.springreactiver2dbc.entity.Todo;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
@RequiredArgsConstructor
public class TodoRepositoryImpl implements TodoRepository {

    private final DatabaseClient databaseClient;

    @Override
    public Mono<Todo> get(Integer id) {
        return databaseClient.select()
                .from(Todo.class)
                .matching(Criteria.where("id").is(id))
                .fetch()
                .first();
    }

    @Override
    public Flux<Todo> getAll() {
        return databaseClient.select()
                .from(Todo.class)
                .fetch()
                .all();
    }

    @Override
    public Mono<Todo> update(final Integer id, final Todo item) {
        return databaseClient
                .update()
                .table(Todo.class)
                .using(item)
                .then()
                .thenReturn(item);
    }

    @Override
    public Mono<Todo> create(final Todo item) {
        return databaseClient.execute("INSERT INTO todo(text) VALUES($1) RETURNING id")
                .bind("$1", item.getText())
                .fetch()
                .first()
                .map(m -> (Integer) m.get("id"))
                .map(id -> item.toBuilder().id(id).build());
    }

    @Override
    public Mono<Void> delete(final Integer id) {
        return databaseClient.delete()
                .from(Todo.class)
                .matching(Criteria.where("id")
                        .is(id))
                .then();
    }
}
