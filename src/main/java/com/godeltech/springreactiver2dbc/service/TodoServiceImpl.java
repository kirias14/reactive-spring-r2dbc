package com.godeltech.springreactiver2dbc.service;

import com.godeltech.springreactiver2dbc.entity.Todo;
import com.godeltech.springreactiver2dbc.repository.TodoRepository;
import com.godeltech.springreactiver2dbc.transformer.TodoTransformer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import com.godeltech.springreactiver2dbc.dto.TodoDto;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class TodoServiceImpl implements TodoService {

    private final TodoRepository todoRepository;
    private final TodoTransformer todoTransformer;

    @Override
    public Flux<TodoDto> getTodos() {
        return todoRepository.getAll()
                .map(todoTransformer::transform);
    }

    @Override
    public Mono<TodoDto> getTodo(final Integer id) {
        return todoRepository.get(id)
                .map(todoTransformer::transform);
    }

    @Override
    public Mono<TodoDto> update(final Integer id, final TodoDto item) {
        final Todo itemToUpdate = todoTransformer.transform(item.toBuilder().id(id).build());
        return todoRepository.update(id, itemToUpdate)
                .map(todoTransformer::transform);
    }

    @Override
    public Mono<TodoDto> create(final TodoDto item) {
        return todoRepository.create(todoTransformer.transform(item))
                .map(todoTransformer::transform);
    }

    @Override
    public Mono<Void> remove(final Integer id) {
        return todoRepository.delete(id);
    }

}
