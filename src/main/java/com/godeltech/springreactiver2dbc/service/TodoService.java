package com.godeltech.springreactiver2dbc.service;

import com.godeltech.springreactiver2dbc.dto.TodoDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TodoService {
    Flux<TodoDto> getTodos();

    Mono<TodoDto> create(TodoDto item);

    Mono<Void> remove(Integer id);

    Mono<TodoDto> getTodo(Integer id);

    Mono<TodoDto> update(Integer id, TodoDto item);
}
