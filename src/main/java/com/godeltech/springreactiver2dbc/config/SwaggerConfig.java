package com.godeltech.springreactiver2dbc.config;

import java.net.URI;
import java.net.URISyntaxException;

import org.springdoc.api.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.WebFilter;

import io.swagger.v3.oas.models.servers.Server;

@Configuration
public class SwaggerConfig {

    private static final String SWAGGER_URL = "/swagger/";
    private static final String SWAGGER_DIRECTORY_INTERNAL = "/webjars/swagger-ui/";

    /**
     * @formatter:off
     * Translates the public paths for swagger-ui.html to internal urls
     * service/swagger/swagger-ui-bundle.js             ->  /webjars/swagger-ui/swagger-ui-bundle.js
     * service/swagger/swagger-ui-standalone-preset.js  ->  /webjars/swagger-ui/swagger-ui-standalone-preset.js
     * and so on
     *
     * If path is "energy__mdd--bs-reassessment-service/v3/api-docs" then change request uri scheme from "http://" to "https://"
     * for correct base server path calculation
     * @formatter:on
     */
    @Bean
    public WebFilter swaggerWebFilter(@Value("${server.servlet.context-path}") final String contextPath) {

        final String swaggerDirectoryPublic = contextPath + SWAGGER_URL;

        return (exchange, chain) -> {
            final ServerHttpRequest request = exchange.getRequest();
            final URI uri = request.getURI();
            final String path = uri.getPath();

            if (path.startsWith(swaggerDirectoryPublic)) {
                final URI internalUri =
                        URI.create(SWAGGER_DIRECTORY_INTERNAL + path.substring(swaggerDirectoryPublic.length()));
                return chain.filter(exchange.mutate().request(request.mutate().uri(internalUri).build()).build());
            }

            return chain.filter(exchange);
        };
    }

}
