package com.godeltech.springreactiver2dbc.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Log4j2
@Component
@RequiredArgsConstructor
public class InitialiseDb implements InitializingBean {

    private final DatabaseClient databaseClient;

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Start create table");
        databaseClient.execute("CREATE TABLE IF NOT EXISTS todo (id serial, \"text\" text, PRIMARY KEY (id))")
                .fetch()
                .rowsUpdated()
                .doOnSuccess(v -> log.info("Created table"))
                .subscribe();
    }
}
