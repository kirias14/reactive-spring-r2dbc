package com.godeltech.springreactiver2dbc.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.core.DatabaseClient;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;

@Configuration
public class DatabaseConfig {

    @Value("${mdd.events.datasource.host}")
    private String host;

    @Value("${mdd.events.datasource.name}")
    private String name;

    @Value("${mdd.events.datasource.username}")
    private String user;

    @Value("${mdd.events.datasource.password}")
    private String password;

    @Bean
    public ConnectionFactory connectionFactory() {
        return new PostgresqlConnectionFactory(PostgresqlConnectionConfiguration.builder()
                .host(host)
                .database(name)
                .username(user)
                .password(password)
                .build());
    }

    @Bean
    public DatabaseClient databaseClient(final ConnectionFactory connectionFactory) {
        return DatabaseClient.create(connectionFactory);
    }

}
