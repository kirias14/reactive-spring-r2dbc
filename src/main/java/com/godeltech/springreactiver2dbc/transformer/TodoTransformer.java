package com.godeltech.springreactiver2dbc.transformer;

import com.godeltech.springreactiver2dbc.dto.TodoDto;
import com.godeltech.springreactiver2dbc.entity.Todo;

public interface TodoTransformer {
    Todo transform(TodoDto item);

    TodoDto transform(Todo item);
}
