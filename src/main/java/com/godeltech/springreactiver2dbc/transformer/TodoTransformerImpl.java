package com.godeltech.springreactiver2dbc.transformer;

import com.godeltech.springreactiver2dbc.dto.TodoDto;
import com.godeltech.springreactiver2dbc.entity.Todo;
import org.springframework.stereotype.Component;

@Component
public class TodoTransformerImpl implements TodoTransformer {

    @Override
    public Todo transform(final TodoDto item) {
        return Todo.builder().id(item.getId()).text(item.getText()).build();
    }

    @Override
    public TodoDto transform(final Todo item) {
        return TodoDto.builder().id(item.getId()).text(item.getText()).build();
    }
}
